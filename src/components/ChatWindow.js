import React, { useState, useEffect , useRef}from 'react';
import './ChatWindow.css';

import Api from '../Api';

import MessageItem from './MessageItem'

import EmojiPicker from 'emoji-picker-react'
import SearchIcon from '@material-ui/icons/Search';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import MicIcon from '@material-ui/icons/Mic';

export default ({user, data}) => {

    const body = useRef();

    // Aqui tudo que o microfone esculta é convertido em texto pelo navegador
    let recognition = null;
    let SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    if(SpeechRecognition !== undefined){
        recognition = new SpeechRecognition();
    }
    // -----------------------------------------------------------------------

    const [emojiOpen, setEmojiOpen] = useState(false);
    const [text, setText] = useState('');
    const [listening, setListening] = useState(false);
    const [list, setList] = useState([]);
    const [users, setUsers] = useState([]);

    useEffect(()=>{

        setList([]);
        let unsub = Api.onChatContent(data.chatId, setList, setUsers);
        return unsub;
    }, [data.chatId])

    useEffect(()=>{
        if(body.current.scrollHeight > body.current.offsetHeight) {
            body.current.scrollTop = body.current.scrollHeight - body.current.offsetHeight;
        }
    }, [list]);

    // Função que seleciona o emoji e escrever no input
    const handleEmojiClick = (e, emojiObject) =>{
        setText( text + emojiObject.emoji )
    }
    // ---------------------------------------------------
    
    // Função que abre a caixa de emoji
    const hadleOpenEmoji = () => {
        
        if(emojiOpen === true){
            setEmojiOpen(false);
        }
        else{
            setEmojiOpen(true);
        }
        
    }
    // ---------------------------------

    // Função que fecha a caixa de emoji
    const hadleCloseEmoji = () => {
        setEmojiOpen(false);
    }
    // ----------------------------------

    const hadleSendClick = () => {
        if(text !== ''){
            
            Api.sendMessage(data, user.id, 'text', text, users);
            setText('');
            setEmojiOpen(false);
        }
    }
    const handleInputKeyUp = (e) =>{
        if(e.keyCode === 13){
            hadleSendClick();
        }
    }

    // Função que programa o Mic pra falar e escrever no input
    const hadleMicClick = () => {
        if(recognition !== null){
            recognition.onstart = () => {
                setListening(true);
            }
            recognition.onend = () => {
                setListening(false);
            }
            recognition.onresult = (e) => {
                setText( e.results[0][0].transcript );
            }

            recognition.start();
        }
    }
    // ----------------------------------------------------------
    

    return(
        <div className="chatWindow">
            <div className="chatWindow--header">
                <div className="chatWindow--headerinfo">
                    <img className="chatWindow--avatar" src={data.image} alt=""/>
                    <div className="chatWindow--name">{data.title}</div>
                </div>

                <div className="chatWindow--headerbuttons">

                    <div className="chatWindow--btn">
                        <SearchIcon className="emoji" />
                    </div>
                    <div className="chatWindow--btn">
                        <AttachFileIcon className="emoji" />
                    </div>
                    <div className="chatWindow--btn">
                        <MoreVertIcon className="emoji" />
                    </div>

                </div>
            </div>
            <div ref={body} className="chatWindow--body">
                {list.map((item, key)=>(
                    <MessageItem
                        key={key}
                        data={item}
                        user={user}
                    />
                ))}
            </div>

            <div className="chatWindow--emojiarea"
            style={{ height: emojiOpen ? '200px' : '0px' }}>
                <EmojiPicker
                    onEmojiClick={handleEmojiClick}
                    disableSearchBar
                    disableSkinTonePicker
                />

            </div>

            <div className="chatWindow--footer">

                <div className="chatWindow--pre">

                    <div 
                        className="chatWindow--btn"
                        onClick={hadleCloseEmoji}
                        style={{ width: emojiOpen?40:0 }}
                    >
                        <CloseIcon className="emoji" />
                    </div>

                    <div 
                        className="chatWindow--btn"
                        onClick={hadleOpenEmoji}
                    >
                        <SentimentVerySatisfiedIcon 
                            className="emoji" 
                            style={{ color: emojiOpen ? '#009688': '#919191'  }} 
                        />
                    </div>

                </div>

                <div className="chatWindow--inputarea">
                    <input 
                        className="chatWindows--input" 
                        type="text"
                        placeholder="Digite uma mensagem"
                        value={text}
                        onChange={e=>setText(e.target.value)}
                        onKeyUp={handleInputKeyUp}
                    />

                </div>
                
                <div className="chatWindow--pos">
                    {text === '' &&
                        <div onClick={hadleMicClick} className="chatWindow--btn">
                            < MicIcon className="emoji" style={{color: listening ? '126ECE' : '#919191'}} />
                        </div>
                    }
                    {text !== '' &&
                        <div onClick={hadleSendClick} className="chatWindow--btn">
                            < SendIcon className="emoji" />
                        </div>
                    }

                </div>
                

            </div>
        </div>
    );
};